<?php 
require_once "connect.php";
require_once "wyszukiwanie.php";
mysqli_query($connect, "SET NAMES utf8");


if (isset($_GET['kategoria'])) {
  $kategoriaSelect = $_GET['kategoria'];


  if (isset($_GET['page_no']) && $_GET['page_no'] != "") {
    $page_no = $_GET['page_no'];
  } else {
    $page_no = 1;
  }
  $total_records_per_page = 6;
  $offset = ($page_no - 1) * $total_records_per_page;

  $result_count = mysqli_query($connect, "SELECT COUNT(*) As total_records FROM `przepis` WHERE kategoria='$kategoriaSelect'");
  $total_records = mysqli_fetch_array($result_count);
  $total_records = $total_records['total_records'];
  $total_no_of_pages = ceil($total_records / $total_records_per_page);
  $second_last = $total_no_of_pages - 1; 

  $query = mysqli_query($connect, "SELECT * FROM `przepis` WHERE kategoria='$kategoriaSelect' LIMIT $offset, $total_records_per_page");



  while ($row = mysqli_fetch_array($query)) {
    $id = $row['ID'];
    $nazwa = $row['nazwa'];
    $skladniki = $row['skladniki'];
    $przygotowanie = $row['przygotowanie'];
    $kategoria = $row['kategoria'];
    $like = $row['likes'];
    $dislike = $row['dislikes'];
    if ($kategoria == $kategoriaSelect) {
        echo <<<END
 <ul class='listRecipes' > 
          <li><span class="id">$id</span><li>
          <li class='list_title' >$nazwa </li> <br> 
          <li>	<h6>Składniki</h6> <li>
          <li class='list_description'>$skladniki </li>  <hr>
          <li >	<h6>Przygotowanie</h6> 	<li>
          <li class='list_description preparing'>$przygotowanie</li> <hr> 
          <li class='list_description' >Kategoria: $kategoria </li>
          <li class='buttonsDelAndEditTd'>
            <br>
            <button class='btn btn-warning del ' type='submit'> Usuń </button> 
            <button type="button" class="btn btn-warning edit" data-toggle="modal" data-target="#exampleModal">
              <span class="glyphicon glyphicon-pencil"></span> Edytuj
            </button>
            <button class='like'>👍$like</button><button class='dislike'>👎$dislike</button>
         </li>
      </ul>
END;
      }
  }
  echo '<br><nav aria-label="..."> <ul class="pagination pagination-sm">';

  if ($total_no_of_pages <= 10) {
    for ($i = 1; $i <= $total_no_of_pages; $i++) {
      if ($i == $page_no) {
        echo "<li class='page-item active'><a class='page-link'>$i</a></li>";
      } else {
        echo "<li><a class='page-link' href='?kategoria=$kategoria&page_no=$i'>$i</a></li>";
      }
    }
  }

  echo '</ul></nav>';
}
 


?>