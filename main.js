let btnEdit = document.querySelectorAll('.edit');
let btnDelete = document.querySelectorAll('.del');
let like = document.querySelector('.like');
let dislike = document.querySelector('.dislike');
btnEdit.forEach( x => {
	x.addEventListener('click', () => {
		let ul = x.parentNode.parentNode;
		let id = ul.querySelector('.id').textContent;
		let nazwa = ul.querySelector('.list_title').textContent;
		let skladniki = ul.querySelector('.list_description').textContent;
		let przygotowanie = ul.querySelector('.preparing').textContent;
		document.querySelector('input[name="id"]').value = id;
		document.querySelector('#nazwaEdit').value = nazwa;
		document.querySelector('#skladnikiEdit').value = skladniki;
		document.querySelector('#przygotowanieEdit').value = przygotowanie;
		console.log(id,nazwa,skladniki,przygotowanie);
	});
});



btnDelete.forEach( x => {
	x.addEventListener('click', () => {
		let ul = x.parentNode.parentNode;
		let id = ul.querySelector('.id').textContent;
		console.log('delete',id);
		$.ajax({
			type: 'POST',
			url: 'delete.php',
			data: {id},
			succes: function(response) {
				window.location = "";
			}
		});
		window.location = "";
	});
});

like.addEventListener('click', (e) => {
	let id = e.target.parentNode.parentNode.querySelector('.id').textContent;
	$.ajax({
		type: 'POST',
		url: './like.php',
		data: {id},
		succes: function(response) {
		window.location = "";
		}
	});
	window.location = "";
})

dislike.addEventListener('click', (e) => {
	let id = e.target.parentNode.parentNode.querySelector('.id').textContent;
	$.ajax({
		type: 'POST',
		url: './dislike.php',
		data: {id},
		succes: function(response) {
		window.location = "";
		}
	});
	window.location = "";
})