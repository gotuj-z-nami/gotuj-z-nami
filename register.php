<?php 
require_once "connect.php";

if(isset($_POST['email'])){
     //udana walidacja
     $wszytsko_OK=true;
     //sprawdzenie nickname
     $nick = $_POST['nick'];
     //sprawdzenie dlugosci nicka
     if(strlen($nick)<3 || (strlen($nick)>20)){
         $wszytsko_OK=false;
         $_SESSION['e_nick']="Nick musi posiadać od 3 do 20 znaków";
     }

     if(ctype_alnum($nick)==false){//sprawdzanie alfanumerycznosci ą,ę <b> itp
         $wszytsko_OK = false;
         $_SESSION['e_nick']="Nick może składać się tylko z liter i cyfr(bez polskich znaków)";
     }

     //Sprawdzanie poprawnosci email
     $email = $_POST['email'];
     $emailB = filter_var($email,FILTER_SANITIZE_EMAIL);

     if((filter_var($emailB,FILTER_VALIDATE_EMAIL)==false)||($emailB!=$email)){
         $wszytsko_OK=false;
         $_SESSION['e_email']="Podaj poprawny adres email";

     }
     //sprawdz poprawnosc hasla
     $haslo1 = $_POST['pass1'];
     $haslo2 = $_POST['pass2'];

     if((strlen($haslo1)<8) || (strlen($haslo1)>20)){
        $wszytsko_OK=false;
        $_SESSION['e_haslo']="Hasło musi posiadać od 8 do 20 znaków";
     }

     if($haslo1!=$haslo2){
        $wszytsko_OK=false;
        $_SESSION['e_haslo']="Podane hasła nie są takie same";

     }
     //czy zaakceptowano regulamin ?
     if(!isset($_POST['regulamin'])){
         $wszytsko_OK = false;
         $_SESSION['e_regulamin']="Potwierdź akceptację regulaminu";
     }
     ///////////////////////////////////////////////////////////////////////////////////////////
    
	
		
/////////////////////////////////////////////////////////////////////////////////////////////
     if($wszytsko_OK==true){//wrzucanie do bazy
         //dziala

         $zapytanie = "INSERT INTO uzytkownicy VALUES (NULL,'$nick', '$haslo1', '$email')";
         mysqli_query($connect,$zapytanie);
         
         echo "Udało sie";
         exit();

     }
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>


    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css">
    <script src="main.js"></script>
    <style>
    .error{
        color:red;
        margin-top:10px;
        margin-bottom:10px;
    }
    </style>
</head>
<body>
    <form method="post" >
        Nazwa użytkownika: <br>
        <label class="sr-only" for="inlineFormInputGroupUsername2">Nazwa użytkownika</label>
  <div class="input-group mb-2 mr-sm-2">
    <div class="input-group-prepend">
      <div class="input-group-text">@</div>
    </div>
    <input type="text" name="nick" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Podaj nazwę użytkownika">
  </div>

        <?php
            if(isset($_SESSION['e_nick'])){
                echo '<div class ="error">'.$_SESSION['e_nick'].'</div>';
                unset($_SESSION['e_nick']);
            }
        ?>
      
        E-mail: <br>  <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Podaj email">
 <br>
        <?php
            if(isset($_SESSION['e_email'])){
                echo '<div class ="error">'.$_SESSION['e_email'].'</div>';
                unset($_SESSION['e_email']);
            }
        ?>



        Twoje hasło: <br><input type="password"  name="pass1" id="inputPassword5" class="form-control" aria-describedby="passwordHelpBlock"><small id="passwordHelpBlock" class="form-text text-muted">
Hasło musi posiadać od 8 do 20 liter.
</small> <br>
        <?php
            if(isset($_SESSION['e_haslo'])){
                echo '<div class ="error">'.$_SESSION['e_haslo'].'</div>';
                unset($_SESSION['e_haslo']);
            }
        ?>       
        Powtórz hasło: <br><input type="password"  name="pass2" id="inputPassword5" class="form-control" aria-describedby="passwordHelpBlock"><br>

        <label><input type="checkbox" name="regulamin" /> Akseptuję regulamin</label>
        <br>
        <?php
            if(isset($_SESSION['e_regulamin'])){
                echo '<div class ="error">'.$_SESSION['e_regulamin'].'</div>';
                unset($_SESSION['e_regulamin']);
            }
        ?> 
        <input type="submit" class="btn btn-warning" value="Zarejestruj się" />
    </form>




</body>
</html>