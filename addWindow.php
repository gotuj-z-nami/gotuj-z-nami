<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</head>

<body>

    <form class="was-validated " action="dodaj.php" method="post">
        <div class="mb-3">
            <label for="validationTextarea">Nazwa</label>
            <textarea class="form-control is-invalid textareaNazwa" name="nazwa" onchange="nameChanged()" id="validationTextarea1" placeholder="Wpisz nazwę przepisu" required></textarea>
            <div class="nameFeedback red">
                Pamiętaj, aby zdania zaczynać dużą literą i kończyć kropką!
            </div>
        </div>


        <div class="mb-3">
            <label for="validationTextarea">Składniki</label>
            <textarea class="form-control is-invalid textareaSkladniki" name="skladniki" onchange="componentsChanged()" id="validationTextarea2" placeholder="Wypisz potrzebne składniki" required></textarea>
            <div class="componentsFeedback red">
                Pamiętaj, aby zdania zaczynać dużą literą i kończyć kropką!
            </div>
        </div>


        <div class="mb-3">
            <label for="validationTextarea">Przygotowanie</label>
            <textarea class="form-control is-invalidmodalTextarea textareaPrzepis" onchange="preparationChanged()" name="przygotowanie" id="validationTextarea3" placeholder="Opisz przygotowanie" required></textarea>
            <div class="preparationFeedback red">
                Pamiętaj, aby zdania zaczynać dużą literą i kończyć kropką!
            </div>
        </div>

        <div class="form-group">
            <select class="custom-select" name="kategoria" required>
                <option value="">Wybierz kategorię</option>
                <option value="Dania i przekąski">Dania i przekąski</option>
                <option value="Ciasta">Ciasta</option>
                <option value="Lody i napoje">Lody i napoje</option>
                <option value="Wege">Wege</option>
                <option value="Kuchnie świata">Kuchnie świata</option>
            </select>
            <div class="invalid-feedback red">Musisz wybrać kategorię do jakiej zalicza się danie! </div>
        </div>

        <input type="submit" class="btn btn-warning add" name="dodaj" value="Dodaj" />
        <br>
        <div class="addFeedbackWrong red">
               BŁĄD!
            </div>
            <div class="addFeedbackWell green">
               Przepis dodany!
            </div>
    </form>



    <script>

        var regexExpressionName = new RegExp("[A-Za-zĄąĆćĘęŁłŃńÓóŚśŹźŻż\s\,\.\:]{1,}");
        var regexExpressionComponents = new RegExp("([A-Z|\s0-9]{0,}[a-ząęćłżóś0-9\-\/\(\)]{0,}\s[a-ząęćóśłżó\s,]{0,})+");
        var regexExpressionPrzygotowanie = new RegExp("([A-Z|\s0-9]{1,}[a-ząęćłżóś]{0,}\s{0,}[a-ząęćóśłżó\s\,:]{0,}[\.\s])+");

        $('.add').on('click', function(event) {
            var name_add = $("#validationTextarea1").val();
            var components_add = $("#validationTextarea2").val();
            var przygotowanie_add = $("#validationTextarea3").val();
            if (regexExpressionName.test(name_add) == false 
            || regexExpressionComponents.test(components_add) == false 
            || regexExpressionPrzygotowanie.test(przygotowanie_add) == false) {
                $(".addFeedbackWrong").css({"display": "inline"});
                $(".addFeedbackWell").css({"display": "none"});
            }
            else{
                $(".addFeedbackWrong").css({"display": "none"});
                $(".addFeedbackWell").css({"display": "inline"});
            }
            }
        );


        function nameChanged() {
            let name_add = $("#validationTextarea1").val();
            if (regexExpressionName.test(name_add) == true)  {
                $(".nameFeedback").css({"display": "none"});
            }
            else{
                $(".nameFeedback").css({"display": "inline"});
            }
        }

        function componentsChanged() {
            let components_add = $("#validationTextarea2").val();
            if ( regexExpressionComponents.test(components_add) == true ) {
                $(".componentsFeedback").css({"display": "none"});
            }
            else{
                $(".componentsFeedback").css({"display": "inline"});
            }
        }

        function preparationChanged() {
            let przygotowanie_add = $("#validationTextarea3").val();
            if (  regexExpressionPrzygotowanie.test(przygotowanie_add) == true) {
                $(".preparationFeedback").css({"display": "none"});
            }
            else{
                $(".preparationFeedback").css({"display": "inline"});
            }
        }
    </script>

    

</body>

</html> 