<?php

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <title>Gotuj z nami - aplikacja kulinarna</title>
</head>

<body>
    <header>
        <p id='headerText'>Gotuj z nami - aplikacja kulinarna</p>
    </header>

    <nav class="navbar navbar-expand-lg navbar-light bg-light" id="navigation">
        <a class="navbar-brand" href="#">Gotuj z nami </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <!-- -->
                    <a class="nav-link" href="http://localhost/projektzespolowy/addWindow.php"> <button type="button" class=" modalButton" data-toggle="modal" data-target="#myModal">Dodaj przepis</button></a>
                </li>

                <li class="nav-item dropdown">

                    <?php

                    $kategorieArray = ['Dania i przekąski', 'Ciasta', 'Lody i napoje', 'Wege', 'Kuchnie świata'];

                    ?>




                    <form action="" method="get" class="fromNav">
                        <select name="kategoria" id="selectNavId" class="selectNav dropdown-men" onchange="this.form.submit()">
                            <?php

                            if (isset($_GET['kategoria'])) {
                                foreach ($kategorieArray as $kat) {
                                    echo '<option class=" optionNav" value="' . $kat . '" ';
                                    if ($kat == $_GET['kategoria'])
                                        echo 'selected ';
                                    echo '>' . $kat . '</option>';
                                }
                            } else {
                                foreach ($kategorieArray as $kat) {
                                    echo '<option class=" optionNav" value="' . $kat . '"  >' . $kat . '</option>';
                                }
                            }

                            ?>
                        </select>
                    </form>

                </li>

            </ul>
            <form class="form-inline my-2 my-lg-0" action="index.php" method="post">
                <input class="form-control mr-sm-2" name ="search" type="search" placeholder="Szukaj przepisów" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0 searchButton" type="submit">Szukaj</button>
            </form>
            <a href="register.php"><button class="btn btn-outline-success my-2 my-sm-0 searchButton" type="submit"> Zarejestruj się</button></a>
   
        </div>
    </nav>
    <main class="mainAll">
     
        <!---------------------------------------------------------------------- -->
        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModal">Edytowanie</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form class="modalBody" action="edit.php" method="post">
                        <h5 class="modalTitle">Nazwa</h5>
                        <input type="text" class="id" name="id" />
                        <input type="text" class="modalTextareaName" id="nazwaEdit" name="nazwaEdit" />
                        <h5 class="modalTitle">Składniki</h5>
                        <textarea class="modalTextarea" id="skladnikiEdit" name="skladnikiEdit"></textarea>
                        <h5 class="modalTitle">Przygotowanie</h5>
                        <textarea class="modalTextarea" id="przygotowanieEdit" name="przygotowanieEdit"></textarea>
                        <select name="kategoriaEdit">
                            <option value="Dania i przekąski">Dania i przekąski</option>
                            <option value="ciasta">Ciasta</option>
                            <option value="Lody i napoje">Lody i napoje</option>
                            <option value="wege">Wege</option>
                            <option value="Kuchnie świata">Kuchnie świata</option>
                        </select>
                        <br>
                        <div class="buttonsEdit">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Anuluj</button>
                            <button type="submit" class="btn btn-warning">Zapisz</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        </div>
        <!---------------------------------------------------------------------- -->

        <div id="containerRecipesList">
            <?php
            require_once "connect.php";
            require_once "wyswietlanie.php";
            require_once "wyszukiwanie.php";
            ?>
        </div>
    </main>


    <footer> Wykonali: Magdalena Kędzia, Marek Kukulski i Artur Nowak
    </footer>

    <script>
        $('.add').on('click', function(event) {
            var name_add = $("#nazwa").val();
            var components_add = $("#skladniki").val();
            var przygotowanie_add = $("#przygotowanie").val();
            var regexExpressionName = new RegExp("[A-Za-zĄąĆćĘęŁłŃńÓóŚśŹźŻż\s\,\.\:]{1,}");
            var regexExpressionComponents = new RegExp("([A-Z|\s0-9]{0,}[a-ząęćłżóś0-9\-\/\(\)]{0,}\s[a-ząęćóśłżó\s,]{0,})+");
            var regexExpressionPrzygotowanie = new RegExp("([A-Z|\s0-9]{1,}[a-ząęćłżóś]{0,}\s{0,}[a-ząęćóśłżó\s\,:]{0,}[\.\s])+");
            if (regexExpressionName.test(name_add) == false || regexExpressionComponents.test(components_add) == false || regexExpressionPrzygotowanie.test(przygotowanie_add) == false) {
                alert("Błędny format! Pamiętaj, aby zdania zaczynać dużą literą i kończyć kropką! :)");
            }
        });
     
      
    </script>
    <script src="main.js"></script>
</body>

</html> 